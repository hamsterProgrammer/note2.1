package hamster.note2;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class NoteAdapter extends BaseAdapter {

    Context cxt;
    LayoutInflater inflater;
    List<Note> notesList;
    private int selectedIndex; //потрібно для виділення елементу списку

    NoteAdapter(Context context, List<Note> objects) {
        cxt = context;
        notesList = objects;
        inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedIndex = -1;
    }

    //(*потрібно для виділення елементу списку)
    public void setSelectedIndex(int index) {
        selectedIndex = index;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return notesList.size();
    }

    @Override
    public Note getItem(int position) {
        return notesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }

    /**
     * Цей метод ідентифікує позицію нотатки.
     * Якщо позицію не знайдено - то повертає -1.
     */
    public int getItemPosition(int noteId) {

        //проходить по всьому списку до тих пір, поки не знайде нотатку з Id = вхідному noteId.
        // А якщо не знайшов таку нотатку, то повертає -1.
        for (int position = 0; position < notesList.size(); position++)
            if (notesList.get(position).getId() == noteId)
        return position;
        return -1;
    }

    private class ViewHolder {
        TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;

        ViewHolder holder;

        if (convertView == null) {
            itemView = inflater.inflate(R.layout.item, parent, false);

            holder = new ViewHolder();
            holder.textView = (TextView) itemView.findViewById(R.id.textViewItem);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        //(*потрібно для виділення елементу списку)
        if (isSelectedItem(position)) {
            holder.textView.setTypeface(Typeface.DEFAULT_BOLD);
        } else {
            holder.textView.setTypeface(Typeface.DEFAULT);
        }

        Note note = notesList.get(position);
        holder.textView.setText(note.getName());
        holder.textView.setBackgroundColor(note.getColor());

        return itemView;
    }

    /*** (*потрібно для виділення вибраного елементу списку) Цей метод перевіряє, чи даний айтем вибраний */
    private boolean isSelectedItem(int position) {
        return selectedIndex != -1 && position == selectedIndex;
    }

    public void updateItems(List<Note> notesList) {
        this.notesList = notesList;
        notifyDataSetChanged();  //make adapter redraw itself
    }
}
