package hamster.note2;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

public class EditingFragment extends Fragment {

    EditText etName, etText;

    NoteDBHelper noteDBHelper;
    SendEmailListener sendEmailListener;
    ColorChangeListener colorChangeListener;

    int clickedNoteId;
    String clickedNoteTitle;
    String clickedNoteText;
    int clickedNoteColor;

    int color;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sendEmailListener = (SendEmailListener) context; //Приводим activity (получаемое на вход м-да onAttach) к типу интерфейса SendEmailListener, чтоб можно было вызывать его м. sendEmail(), реализованный в MainActivity.
        colorChangeListener = (ColorChangeListener) context;
        color = ContextCompat.getColor(context, R.color.white);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Report that this fragment would like to participate in MainActivity menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.editing_fragment, null);

        etName = (EditText) view.findViewById(R.id.etNameOfNote);
        etText = (EditText) view.findViewById(R.id.etTextOfNote);

        noteDBHelper = new NoteDBHelper(getActivity());

        //отримуємо дані про клікнуту нотатку з MyListFragment - дістаємо їх з аргументів, які записували в м-ді newInstance
        if (getArguments() != null && getArguments().getString("name") != null) {

            clickedNoteId = getArguments().getInt("id");
            clickedNoteTitle = getArguments().getString("name");
            clickedNoteText = getArguments().getString("text");
            clickedNoteColor = noteDBHelper.getColor(clickedNoteId); //колір дістаємо напряму з БД.

            //відображаємо назву та текст клікнутої нотатки в едітТекстах
            editNote(clickedNoteTitle, clickedNoteText, clickedNoteColor);
            //відображаємо колір клікнутої нотатки в бекГраунді
            etName.setBackgroundColor(clickedNoteColor);
            etText.setBackgroundColor(clickedNoteColor);
        }
        return view;
    }

    /***М. newInstance (*нужен для обработки смены ориентации экрана)- СОЗДАЕТ ЭКЗЕМПЛЯР EditingFragment И ЗАПИСЫВАЕТ В ЕГО АРГУМЕНТЫ ЧИСЛО, ПРИШЕДШЕЕ НА ВХОД МЕТОДУ.
     Это число - позиция выбранного элемента из списка заголовков. */
    public static EditingFragment newInstance(int id, String clickedNoteName, String clickedNoteText, int clickedNoteColor) {
        EditingFragment editingFragment = new EditingFragment();
        Bundle arguments = new Bundle();
        arguments.putInt("id", id);
        arguments.putString("name", clickedNoteName);
        arguments.putString("text", clickedNoteText);
        arguments.putInt("color", clickedNoteColor);
        editingFragment.setArguments(arguments); //М. setArguments - позволяет записать аргументы, а getArguments – считать.
        return editingFragment;
    }

    /***м. getClickedNoteId (*нужен для обработки смены ориентации экрана) - достает из аргументов id выбранного элемента из списка заголовков. */
    int getClickedNoteId() {
        int id = getArguments().getInt("id", -1);
        return id;
    }

    /*** Отримує клікнуту нотатку (її назву та текст) і вставляє її в editText-и */
    public void editNote(String name, String text, int clickedNoteColor) {

        String noteTitle = name;
        String noteText = text;
        color = clickedNoteColor;

        etName.setText(noteTitle);
        etText.setText(noteText);
    }

    //ActionBar for EditingFragment (элементы из хмл этого екшнБара будут появляться при выводе фрагмента на экран)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_editing, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /*** Перевіряємо:
     - якщо юзер створює нову нотатку, то зберігаємо її в БД.
     - якщо юзер редагує існуючу нотатку, то оновлюємо її в БД.
     Цей метод повертає новостворену або відредаговану нотатку. */
    public int addORupdateNote() {

        int newOrUpdatedNoteId = -1;

        //якщо не прийшли ніякі аргументи (значить, юзер створює нову нотатку)
        Bundle arguments = getArguments();
        if (arguments == null || arguments.getInt("id") <= 0) {

            MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.sound);
            mediaPlayer.start();

            //якщо etName не пустий, то ДОДАЄМО НОВУ НОТАТКУ У БД
            if (etName.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "You forgot to enter title!", Toast.LENGTH_LONG).show();
            } else {
                String noteName = etName.getText().toString();
                String noteText = etText.getText().toString();
                int noteColor = color;

                Note newNote = new Note(noteName, noteText, noteColor);
                newOrUpdatedNoteId = noteDBHelper.addNote(newNote); //додаємо нову нотатку і відразу отримуємо її id (потрібно для завдання: новостворена нотатка має відразу бути обраною (жирною))
            }

        } else { //якщо був натиснутий айтем, ЗНАЧИТЬ, ЮЗЕР РЕДАГУЄ НОТАТКУ

            MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.sound);
            mediaPlayer.start();

            if (etName.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "You forgot to enter title!", Toast.LENGTH_LONG).show();
            } else {
                String editingNoteName = etName.getText().toString();
                String editingNoteText = etText.getText().toString();
                int editingNoteColor = color;

                Note editingNote = new Note(clickedNoteId, editingNoteName, editingNoteText, editingNoteColor);
                noteDBHelper.updateNote(editingNote);

                newOrUpdatedNoteId = editingNote.getId();
            }
        }

        return newOrUpdatedNoteId;
    }

    /*** Обробка кліку на item_color в ActionBar:
     відкриваємо діалог з перечисленими кольорами та змінюємо колір нотатки відповідно до обраного юзером кольору */
    public void colorСhange () {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select color: ");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Pink");
        arrayAdapter.add("Blue");
        arrayAdapter.add("Green");

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String sColor = arrayAdapter.getItem(which);

                if (sColor.equals("Pink")) {
                    color = ContextCompat.getColor(getActivity(), R.color.lightpink);
                    etName.setBackgroundColor(getResources().getColor(R.color.lightpink)); //оновляємо колір в едітТекстах
                    etText.setBackgroundColor(getResources().getColor(R.color.lightpink));
                    reloadColor(color); //оновлюємо колір нотатки в майлістФрагменті і в БД

                } else if (sColor.equals("Blue")) {
                    color = ContextCompat.getColor(getActivity(), R.color.lightblue);
                    etName.setBackgroundColor(getResources().getColor(R.color.lightblue));
                    etText.setBackgroundColor(getResources().getColor(R.color.lightblue));
                    reloadColor(color);

                } else if (sColor.equals("Green")) {
                    color = ContextCompat.getColor(getActivity(), R.color.lightgreen);
                    etName.setBackgroundColor(getResources().getColor(R.color.lightgreen));
                    etText.setBackgroundColor(getResources().getColor(R.color.lightgreen));
                    reloadColor(color);
                }
            }
        });
        builderSingle.show();
    }

    /*** Обробка кліку на item_email: відкриваємо діалог з запитом на відсилання емейлу.*/
    public void emailDialog () {
        //Пишемо діалог
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle("Hey!");
        dialogBuilder.setMessage("Do you want to send email?");
        dialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Надсилаємо емейл
                    String noteName = etName.getText().toString();
                    String noteText = etText.getText().toString();

                sendEmailListener.sendEmail(noteName, noteText);
                }
        });

         dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    });

    dialogBuilder.show();

    }

    //Цей метод оновлює колір нотатки в БД і в майЛістФрагменті
    public void reloadColor (int color) {
        String editingNoteName = etName.getText().toString();
        String editingNoteText = etText.getText().toString();
        int editingNoteColor = color;
        Note editingNote = new Note(clickedNoteId, editingNoteName, editingNoteText, editingNoteColor);
        noteDBHelper.updateColor(editingNote.getId(), color);
        colorChangeListener.colorChanged();
    }

}


