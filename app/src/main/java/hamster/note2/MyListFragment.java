package hamster.note2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MyListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private NoteAdapter noteAdapter;
    NoteDBHelper noteDBHelper;
    BtnAddListener btnAddListener;
    MyOnItemClickListener myOnItemClickListener;
    Context context;
    ListView listView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        btnAddListener = (BtnAddListener) context; //Приводим activity (получаемое на вход м-да onAttach) к типу интерфейса BtnAddListener, чтоб можно было вызывать его м. btnAddClicked(), реализованный в MainActivity. Теперь BtnAddListener ссылается на MainActivity.
        myOnItemClickListener = (MyOnItemClickListener) context; //Приводим activity (получаемое на вход м-да onAttach) к типу интерфейса myOnItemClickListener
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Report that this fragment would like to participate in MainActivity menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, null);

        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        noteDBHelper = new NoteDBHelper(context);

        //initialize/create RoutineBaseAdapter with EMPTY LIST
        noteAdapter = new NoteAdapter(context, new ArrayList<Note>());
        listView.setAdapter(noteAdapter);

        //load all notes from DB and update items in adapter
        updateItemsInAdapter();

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //КОЛИ КЛІКАЄШ НА АЙТЕМ, ВІДКРИВАЄТЬСЯ ФРАГМЕНТ EditingFragment ЗІ ЗМІСТОМ НОТАТКИ.
        //ЦЮ НОТАТКУ МОЖНА РЕДАГУВАТИ І ОНОВЛЯТИ В БД

        //CALLBACK: вызываем метод btnAddClicked() интерфейса BtnAddListener, который реализован в MainActivity
        Note clickedNote = noteAdapter.getItem(position);
        myOnItemClickListener.myListFragmentOnItemClicked(clickedNote, position);

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        //КОЛИ ДОВГО КЛІКАЄШ НА АЙТЕМ, ВІН ВИДАЛЯЄТЬСЯ З БД
        final int notePosition = position;

        //Пишемо діалог
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle("Hey!");
        dialogBuilder.setMessage("Delete note?");
        dialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Видаляємо нотатку з БД
                Note clickedNote = noteAdapter.getItem(notePosition);
                noteDBHelper.deleteNote(clickedNote);
                updateItemsInAdapter();
            }
        });

        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialogBuilder.show();

        return true;
    }

    /*** LOAD ALL NOTES FROM DB and update items in adapter. Then select note by its position (виділяє нотатку) */
    public void updateItemsInAdapterByNote(int noteId) {
        updateItemsInAdapter();
        int notePosition = noteAdapter.getItemPosition(noteId); //отримуємо позицію нотатки в адаптері (в list view)

        View notesItemView = listView.getAdapter().getView(notePosition, null, null); //отримуємо view цієї нотатки
        listView.performItemClick(notesItemView, notePosition, listView.getAdapter().getItemId(notePosition)); //клікаємо (програмно) на цю нотатку

        List<Note> noteList = noteDBHelper.getNotesList();
        noteAdapter.updateItems(noteList); //update adapter with new item
    }

    /*** LOAD ALL NOTES FROM DB and update items in adapter 1 */
    public void updateItemsInAdapter(int position) {
        List<Note> noteList = noteDBHelper.getNotesList();
        noteAdapter.updateItems(noteList);
        noteAdapter.setSelectedIndex(position);
    }

    /*** LOAD ALL NOTES FROM DB and update items in adapter 2 */
    public void updateItemsInAdapter() {
        List<Note> noteList = noteDBHelper.getNotesList();
        noteAdapter.updateItems(noteList);
    }

    //ActionBar for EditingFragment (элементы из хмл этого екшнБара будут появляться при выводе фрагмента на экран)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Обработка нажатия на элементы ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //КОЛИ КЛІКАЄШ НА item_add, то ВІДКРИВАЄТЬСЯ ФРАГМЕНТ EditingFragment, В ЯКОМУ МОЖНА СТВОРИТИ НОВУ НОТАТКУ
            case R.id.item_add:
                //CALLBACK: вызываем метод btnAddClicked() интерфейса BtnAddListener, который реализован в MainActivity
                btnAddListener.btnAddClicked();
                break;
        }
        return true;
    }


}
