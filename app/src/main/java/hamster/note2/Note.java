package hamster.note2;


public class Note {
    int id;
    String name;
    String text;
    int color;

    Note (int id, String name, String text) {
        this.id = id;
        this.name = name;
        this.text = text;
    }

    Note (String name, String text, int color) {
        this.name = name;
        this.text = text;
        this.color = color;
    }

    Note (int id, String name, String text, int color) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.color = color;
    }


    public int getId () {
        return id;
    }

    public String getName () {
        return name;
    }

    public String getText () {
        return text;
    }

    public int getColor () {
        return color;
    }

}
