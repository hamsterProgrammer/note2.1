package hamster.note2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class NoteDBHelper extends SQLiteOpenHelper {

    private static final String NOTES_DATABASE = "notes.db";
    private static final String NOTES_TABLE = "notesTable";

    public NoteDBHelper(Context context) {
        super(context, NOTES_DATABASE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + NOTES_TABLE + " ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "text text,"
                + "color integer"
                + ");"
        );
    }

    public int addNote(Note note) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", note.getName());
        contentValues.put("text", note.getText());
        contentValues.put("color", note.getColor());

        SQLiteDatabase db = getWritableDatabase();
        long addedNoteId = db.insert(NOTES_TABLE, null, contentValues); //вставляємо нотатку в нашу таблиць та отримуємо її id (його повертає нам м. insert)
        db.close();

        int addedNoteIdInt = (int) addedNoteId;

        return addedNoteIdInt;
    }

    public List<Note> getNotesList() {
        SQLiteDatabase db = getWritableDatabase();
        List<Note> noteList = new ArrayList<>();

        //Для чтения используется м. query. На вход ему подается: имя таблицы, список запраш.полей, условия выборки, группировка, сортировка.
        //М. query возвращает объект класса Cursor (это набор строк в табличном виде):
        Cursor c = db.query(NOTES_TABLE, null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки (если в выборке нет строк, вернется false)
        if (c.moveToFirst()) {
            // определяем номеров столбцов по имени в выборке
            int idColumnIndex = c.getColumnIndex("id");
            int nameColumnIndex = c.getColumnIndex("name");
            int textColumnIndex = c.getColumnIndex("text");
            int colorColumnIndex = c.getColumnIndex("color");

            do {
                // получаем значения по номерам столбцов и пишем в обєкт
                int idOfNote = c.getInt(idColumnIndex);
                String nameOfNote = c.getString(nameColumnIndex);
                String textOfNote = c.getString(textColumnIndex);
                int colorOfNote = c.getInt(colorColumnIndex);

                Note note = new Note(idOfNote, nameOfNote, textOfNote, colorOfNote);
                noteList.add(note);

                // переход на следующую строку, а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        }

        c.close();
        db.close();

        return noteList;
    }

    public int getColor(int noteId) {

        int colorOfNote = 0;

        SQLiteDatabase db = getWritableDatabase(); // подключаемся к БД

        String[] columns = new String[]{"color"};
        String[] stringNoteId = new String[]{String.valueOf(noteId)};

        Cursor c = db.query(NOTES_TABLE, columns, "id = ?", stringNoteId, null, null, null);

        // ставим позицию курсора на первую строку выборки (если в выборке нет строк, вернется false)
        if (c.moveToFirst()) {
            int colorColumnIndex = c.getColumnIndex("color"); //определяем номер столбца по имени в выборке

            do {
                colorOfNote = c.getInt(colorColumnIndex); //получаем значение по номеру столбца и пишем в обєкт

            }
            while (c.moveToNext()); //переход на следующую строку, а если следующей нет (текущая - последняя), то false - выходим из цикла
        }

        return colorOfNote;
    }

    public void updateColor(int noteId, int noteColor) {
        ContentValues cv = new ContentValues(); // создаем объект для данных
        SQLiteDatabase db = getWritableDatabase(); // подключаемся к БД

        // получаем необходимые данные из едитТекстов
        String stringId = String.valueOf(noteId);

        // подготовим значения для обновления
        cv.put("color", noteColor);

        // обновляем по id
        db.update(NOTES_TABLE, cv, "id = ?", new String[]{stringId});
    }

    public void updateNote(Note note) {
        // создаем объект для данных
        ContentValues cv = new ContentValues();
        // получаем данные из едитТекстов
        int id = note.getId();
        String stringId = String.valueOf(id);
        String name = note.getName();
        String text = note.getText();
        int color = note.getColor();

        SQLiteDatabase db = getWritableDatabase(); // подключаемся к БД

        // подготовим значения для обновления
        cv.put("id", id);
        cv.put("name", name);
        cv.put("text", text);
        cv.put("color", color);

        // обновляем по id
        db.update(NOTES_TABLE, cv, "id = ?", new String[]{stringId});

        db.close();
    }

    public void deleteNote(Note note) {
        SQLiteDatabase db = getWritableDatabase();
        int id = note.getId();
        db.delete(NOTES_TABLE, "id = " + id, null);
        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}


