package hamster.note2;


public interface SendEmailListener {
    void sendEmail(String noteName, String noteText);
}
