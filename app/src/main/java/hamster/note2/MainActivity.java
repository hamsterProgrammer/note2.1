package hamster.note2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements BtnAddListener, MyOnItemClickListener, SendEmailListener, ColorChangeListener {

    //ця змінна повідомлятиме нам: Activity показує  ListFragment (заголовки) разом з EditingFragment (зміст) чи без.
    // Це співпадатиме з гориз. і ветик. орієнтацією.
    boolean isHorizontalOrientation = true;

    int clickedNoteColor;

    //для onSaveInstanceState
    int id;
    String clickedNoteName;
    String clickedNoteText;
    int clickedNotePosition; //ця змінна для завдання: При зміні орієнтації з Вертик. на Гориз., обраний елемент має залишатися виділеним (жирним).

    EditingFragment editingFragment;
    private static final String EDITING_FRAGMENT_TAG = "EditingFragmentTag";

    //В onCreate ХЕНДЛИМО ЗМІНУ ОРІЄНТАЦІЇ ЕКРАНУ: 1)отримуємо дані клікнутої нотатки з savedInstanceState. 2) перевіряємо, в якій орієнтації зараз екран:
    //  в горизонт. - показуємо два фрагменти на одному екрані (ListFragment (заголовки) разом з EditingFragment (зміст)).
    //  в вертикальній - (ЯКЩО АКТІВІТІ ПЕРЕСТВОРЮЄТЬСЯ ПІСЛЯ ЗМІНИ ОРІЄНТАЦІЇ ЕКРАНУ) - показуємо EditingActivity (передавши їй дані нотатки. А вона відобразить нам EditingFragment).
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            id = savedInstanceState.getInt("id");
            clickedNoteName = savedInstanceState.getString("name");
            clickedNoteText = savedInstanceState.getString("text");
            clickedNotePosition = savedInstanceState.getInt("clickedNotePosition");
            NoteDBHelper noteDBHelper = new NoteDBHelper(this);
            clickedNoteColor = noteDBHelper.getColor(id); //колір нотатки дістаємо напряму з бд (а не з savedInstanceState). Це потрібно, щоб при зміні орієнтації екрану в процесі редагування нотатки показувався правильний колір - той, який був виставлений юзером (а не колишній).
        }

        //ця змінна повідомлятиме: Activity показує ListFragment разом з EditingFragment чи без. Це співпадає з гориз. і ветик. орієнтацією.
        isHorizontalOrientation = (findViewById(R.id.container_for_editingFragment) != null);

        //ЯКЩО ГОРИЗОНТАЛЬНА ОРІЄНТАЦІЯ, то показуємо два фрагменти на одному екрані
        if (isHorizontalOrientation) {
            //выкликаємо м. showEditing, передавши йому клікнуту нотатку - а він відобразить нам необхідний зміст на екрані.
            showTwoFragments(id, clickedNoteName, clickedNoteText, clickedNoteColor, clickedNotePosition);
        }

        //ЯКЩО АКТІВІТІ ПЕРЕСТВОРЮЄТЬСЯ ПІСЛЯ ЗМІНИ ОРІЄНТАЦІЇ ЕКРАНУ, І ОРІЄНТАЦІЯ ВЕРТИКАЛЬНА, то показуємо EditingActivity
        if (isConfigurationChanged(savedInstanceState) && !isHorizontalOrientation) {
            //выкликаємо м. showEditing, передавши йому клікнуту нотатку - а він відобразить нам необхідний зміст на екрані.
            showEditingActivity(id, clickedNoteName, clickedNoteText, clickedNoteColor);
            }
        }

    /**
     * Цей метод показує два фрагменти (MyListFragment та EditingFragment) в одному Актівіті.
     * - Створює EditingFragment з даними клікнутої нотатки (які ідуть йому на вхід)
     * (Спочатку шукає EditingFragment. Якщо не знаходить, або знаходить, але відображаючий дані по іншій позиції -
     * то створює фрагмент наново, передаючи йому потрібну позицію, та поміщає його в контейнер).
     * - Оновляє MyListFragment.
     */
        private void showTwoFragments (int id, String clickedNoteName, String clickedNoteText, int clickedNoteColor, int clickedNotePosition) {

                EditingFragment editingFragment = (EditingFragment) getSupportFragmentManager().findFragmentByTag(EDITING_FRAGMENT_TAG);

                if (editingFragment == null || editingFragment.getClickedNoteId() != id) {
                    editingFragment = EditingFragment.newInstance(id, clickedNoteName, clickedNoteText, clickedNoteColor);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container_for_editingFragment, editingFragment, EDITING_FRAGMENT_TAG).commit();

                    MyListFragment myListFragment = (MyListFragment) getSupportFragmentManager().findFragmentById(R.id.myLisFragment);
                    myListFragment.updateItemsInAdapter(clickedNotePosition); //При зміні орієнтації з Вертик. на Гориз., обраний елемент має залишатися виділеним (жирним).
                }
        }

    /**
     * Цей метод показує зміст нотатки (EditingFragment) в окремому EditingActivity:
     *  викликаємо EditingActivity і передаємо їй усі дані клікнутої нотатки (які ідуть на вхід м-ду).
     */
    private void showEditingActivity(int id, String clickedNoteName, String clickedNoteText, int clickedNoteColor) {

            EditingFragment editingFragment = (EditingFragment) getSupportFragmentManager().findFragmentByTag(EDITING_FRAGMENT_TAG);
            if (editingFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(editingFragment).commit();
            }

            Intent intent = new Intent(this, EditingActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("clickedNoteName", clickedNoteName);
            intent.putExtra("clickedNoteText", clickedNoteText);
            intent.putExtra("clickedNoteColor", clickedNoteColor);
            startActivityForResult(intent, 1);
        }


    //Цей метод перевіряє, чи була змінена орієнтація
    private boolean isConfigurationChanged(Bundle bundle) {
        return bundle != null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        MyListFragment myListFragment = (MyListFragment) getSupportFragmentManager().findFragmentById(R.id.myLisFragment);
        myListFragment.updateItemsInAdapter();
    }

    @Override //Створюємо ActionBar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override //ОБРОБЛЯЄМО КЛІКИ НА ЕЛЕМЕНТИ ACTIONBAR
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //КОЛИ КЛІКАЄШ НА item_save, ТО В БД ДОДАЄТЬСЯ (або оновлюється) НОТАТКА і В MyListFragment оновляються дані в адаптері (нові назва і текст нотатки)
            case R.id.item_save:
                editingFragment = (EditingFragment) getSupportFragmentManager().findFragmentByTag(EDITING_FRAGMENT_TAG);
                if (editingFragment != null) {
                    int noteId = editingFragment.addORupdateNote(); //додаємо або оновляємо нотатку в БД + отримуємо id новоствореної або доданої нотатки

                    if (noteId != -1) {
                        MyListFragment myListFragment = (MyListFragment) getSupportFragmentManager().findFragmentById(R.id.myLisFragment);
                        myListFragment.updateItemsInAdapterByNote(noteId); //оновляємо MyListFragment + відразу виділяємо новододану нотатку
                    }
                }
                break;

            //КОЛИ КЛІКАЄШ НА item_color, ТО ВІДКРИВАЄТЬСЯ ДІАЛОГ З ПЕРЕЧИСЛЕНИМИ КОЛЬОРАМИ
            case R.id.item_color:
                editingFragment = (EditingFragment) getSupportFragmentManager().findFragmentByTag(EDITING_FRAGMENT_TAG);
                if (editingFragment != null) {
                    editingFragment.colorСhange();
                }
                break;

            //КОЛИ КЛІКАЄШ НА item_email, ТО ВІДКРИВАЄТЬСЯ ДІАЛОГОВЕ ВІКНО З ЗАПИТОМ НА ВІДСИЛАННЯ ЕМЕЙЛУ
            case R.id.item_email:
                editingFragment = (EditingFragment) getSupportFragmentManager().findFragmentByTag(EDITING_FRAGMENT_TAG);
                if (editingFragment != null) {
                    editingFragment.emailDialog();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override //callback***Цей метод обробляє клік на заголовок нотатки
    public void myListFragmentOnItemClicked(Note clickedNote, int clickedNotePosition) {
        id = clickedNote.getId();
        clickedNoteName = clickedNote.getName();
        clickedNoteText = clickedNote.getText();
        clickedNoteColor = clickedNote.getColor();

        this.clickedNotePosition = clickedNotePosition; //При зміні орієнтації з Вертик. на Гориз., обраний елемент має залишатися виділеним.

        if (isHorizontalOrientation) {
            showTwoFragments(id, clickedNoteName, clickedNoteText, clickedNoteColor, clickedNotePosition);
        } else {
            showEditingActivity(id, clickedNoteName, clickedNoteText, clickedNoteColor);
        }
    }

    //callback.***Цей обробляє клік на кн. додати */
    @Override
    public void btnAddClicked() {

        if (isHorizontalOrientation) {
            //оновляємо дані нотатки, які збережуться в onSaveInstanceState
            // (щоб якщо нотатку будуть відкривати в едітінгАктівіті, вона була пуста а не зі значеннями попередньо вибраної нотатки)
            id = -1;
            clickedNoteName = null;
            clickedNoteText = null;
            clickedNoteColor = -1;

            //створюємо і показуємо едітінгФрагмент з пустою нотаткою
            EditingFragment editingFragment = EditingFragment.newInstance(-1, null, null, -1);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_for_editingFragment, editingFragment, EDITING_FRAGMENT_TAG);
            fragmentTransaction.commit();

        } else {
            Intent intent = new Intent(this, EditingActivity.class);
            startActivityForResult(intent, 1);
        }
    }

    //callback.***Цей метод надсилає емейл */
    @Override
    public void sendEmail(String noteName, String noteText) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, noteName);
        emailIntent.putExtra(Intent.EXTRA_TEXT, noteText);

        this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    //callback.***Цей метод оновлює MyListFragment після того, як був змінений колір нотатки в ЕditingFragment*/
    @Override
    public void colorChanged() {
        MyListFragment myListFragment = (MyListFragment) getSupportFragmentManager().findFragmentById(R.id.myLisFragment);
        myListFragment.updateItemsInAdapter();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("id", id);
        outState.putString("name", clickedNoteName);
        outState.putString("text", clickedNoteText);
        outState.putInt("clickedNotePosition", clickedNotePosition); //ця змінна для завдання: При зміні орієнтації з Вертик. на Гориз., обраний елемент має залишатися виділеним (жирним).
    }


}
