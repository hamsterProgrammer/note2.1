package hamster.note2;


public interface MyOnItemClickListener {
    void myListFragmentOnItemClicked (Note clickedNote, int clickedNotePosition);
}
