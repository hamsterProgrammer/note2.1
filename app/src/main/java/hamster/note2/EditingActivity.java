package hamster.note2;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.MenuItem;

//ДОБАВЛЯЕМ УЧЕТ ОРИЕНТАЦИИ ЭКРАНА. В ВЕРТИКАЛЬНОЙ ОРИЕНТАЦИИ MainActivity БУДЕТ ОТОБРАЖАТЬ ТОЛЬКО названия нотаток (MyListFragment).
//ДЛЯ ЭТОГО ФРАГМЕНТ С СОДЕРЖИМЫМ (EditingFragment) ВЫНЕСЕМ В ОТДЕЛЬНОЕ АКТИВИТИ - EditingActivity (прописать это активити в манифесте).

//А также надо создать два лойаута для activity_main.xml:
// 1)res/layout c activity_main.xml c одним <fragment> для MyListFragment.
//2) res/layout-land c activity_main.xml c <fragment> для MyListFragment и с контейнером для EditingFragment.

public class EditingActivity extends AppCompatActivity implements BtnAddListener, BtnSaveListener, SendEmailListener, ColorChangeListener {

    EditingFragment editingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//Проверяем: если ориентация горизонтальная - то надо показывать и заголовки, и содержимое.
// А значит, надо вернуться в MainActivity (которая показывает и заголовки, и содержимое) - поэтому закрываем EditingActivity.
//Т.к. EditingActivity вызвано из MainActivity, то после finish попадем в MainActivity. Т.к. MainActivity хранит позицию выбранного заголовка (независимо от ориентации), содержимое отобразится то же, что было в DetailsActivity.
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
            return;
        }

//Проверяем: если savedInstanceState == null (значит, это аctivity создается впервые, а не пересоздается после смены ориентации),
// ТО СОЗДАЕМ EditingFragment, ИСПОЛЬЗУЯ ПОЗИЦИЮ ИЗ ИНТЕНТА (пришедшего из MainActivity) И ПОМЕЩАЕМ ЕГО В EditingActivity.
// (Фрагмент  создаем только при создании аctivity а при пересоздании нет, потому что...
//..с-ма сама умеет пересоздавать существующие фрагменты при смене ориентации, сохраняя при этом аргументы фрагмента).
        if (savedInstanceState == null) {

            int id = getIntent().getIntExtra("id", 0);
            String clickedNoteName = getIntent().getStringExtra("clickedNoteName");
            String clickedNoteText = getIntent().getStringExtra("clickedNoteText");
            int clickedNoteColor = getIntent().getIntExtra("clickedNoteColor", 0);

            editingFragment = EditingFragment.newInstance(id, clickedNoteName, clickedNoteText, clickedNoteColor);
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, editingFragment).commit(); //Android.R.id.content - дает вам корневой элемент представления, не зная его фактического идентификатора.
        }
    }

    //Обработка нажатия на элементы ActionBar в EditingActivity.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //КОЛИ КЛІКАЄШ НА item_save, ТО В БД ДОДАЄТЬСЯ (або оновлюється) НОТАТКА І ВІДКРИВАЄТЬСЯ MyListFragment, в якому оновляються дані в адаптері (нові назва і текст нотатки)
            case R.id.item_save:
                int noteId = editingFragment.addORupdateNote(); //додаємо або оновляємо нотатку в БД + отримуємо id новоствореної або доданої нотатки
                if (noteId != -1) { //якщо нотатка не пуста
                    //в MainActivity робим startActivityForResult (у всіх випадках, де відкриваємо едітінгАктівіті. Обробка онАктівіті резулт
                    // - провіряємо реквест код і якщо він значить, що едітінгАктівіті зберегло дані, то меінАктівіті оновляє лістФрагмент)
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            //КОЛИ КЛІКАЄШ НА item_color, ТО ВІДКРИВАЄТЬСЯ ДІАЛОГ З ПЕРЕЧИСЛЕНИМИ КОЛЬОРАМИ
            case R.id.item_color:
                editingFragment.colorСhange();
                break;
            //КОЛИ КЛІКАЄШ НА item_email, ТО ВІДКРИВАЄТЬСЯ ДІАЛОГОВЕ ВІКНО З ЗАПИТОМ НА ВІДСИЛАННЯ ЕМЕЙЛУ
            case R.id.item_email:
                editingFragment.emailDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    //CALLBACK
    @Override
    public void btnAddClicked() {

    }

    //CALLBACK
    @Override
    public void btnSaveClicked() {

    }

    //CALLBACK
    @Override
    public void sendEmail(String noteName, String noteText) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, noteName);
        emailIntent.putExtra(Intent.EXTRA_TEXT, noteText);

        this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    //CALLBACK
    @Override
    public void colorChanged() {

    }
}
